import pathlib
from setuptools import setup, find_packages

root = pathlib.Path(__file__).parent

ns = {}
with (root / 'felt' / '__version__.py').open() as f:
    exec(f.read(), ns)

with open('README.rst') as f:
    readme = f.read()

setup(
    name='felt',
    version=ns['__version__'],
    description="Estimate flow on paths.",
    long_description=readme,
    author="Eric S. Talbot",
    url="https://gitlab.com/ericstalbot/felt",
    packages=find_packages(exclude=['docs', 'tests']),
    python_requires='>=3.7, <4',
    install_requires=[],
    extras_require={
        'dev': [
            'pytest',
            'pytest-cov',
            'mypy',
            'pylint',
            'invoke',
            'wheel',
            'sphinx',
            'sphinx_rtd_theme',
            'twine',
        ]
    }
)   

