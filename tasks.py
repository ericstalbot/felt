from invoke import task

@task
def check(c):
    with c.cd('docs'):
        c.run('sphinx-build -M doctest . _build -W')
    c.run("pylint felt")
    c.run("mypy felt")
    c.run("pytest")

@task 
def build(c):
    with c.cd('docs'):
        c.run('sphinx-build -M html . _build -W')
    c.run('python setup.py bdist_wheel')

@task 
def upload(c):
    c.run('twine upload --skip-existing dist/*.whl')
