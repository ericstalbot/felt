import pytest
from felt.routing import shortest_path, NoPath
from felt.network import Network, Node, Way, Path

def test_shortest_path():
    network = Network(
        [
            Node(1, 0.0, 0.0),
            Node(2, 1.0, 0.0),
            Node(3, 2.0, 0.0),
            Node(4, 3.0, 0.0),
            Node(5, 0.0, 1.0),
            Node(6, 3.0, 1.0)
        ],
        [
            Way([1, 2, 3, 4], oneway=True),
            Way([5, 2], oneway=True),
            Way([3, 6], oneway=True)
        ]
    )

    assert network.shortest_path(1, 6) == network.path([1, 2, 3, 6])

def test_nopath():
    network = Network(
        [
            Node(1, 0.0, 0.0),
            Node(2, 1.0, 0.0),
            Node(3, 2.0, 0.0),
            Node(4, 3.0, 0.0),
        ],
        [
            Way([1, 2], oneway=True),
            Way([3, 4], oneway=True)
        ]
    )

    print(network.links)

    with pytest.raises(NoPath):
        network.shortest_path(1, 4)

    with pytest.raises(NoPath):
        network.shortest_path(2, 1)

    with pytest.raises(NoPath):
        network.shortest_path(2, 3)

    with pytest.raises(NoPath):
        network.shortest_path(1, 3)

    assert network.shortest_path(1, 2) == network.path([1, 2])
