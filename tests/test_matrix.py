from felt.matrix import Matrix, Vector




def test_matrix_1():
    A = Matrix(
        [
            [2, 1, 0, 0], 
            [0, 0, 1, 1], 
            [1, 0, 1, 0]
        ]
    )

    b = Vector([1, 2, 3, 4])

    res = A @ b
    
    assert res.equals(Vector([4, 7, 4]))