import pytest
from felt.movement import Movement
from felt.network import Path

@pytest.mark.parametrize('movement,path,count',[
    ([1, 2], [1, 2],  1),
    ([1], [1],  1),
    ([..., 1], [1],  1), 
    ([1, ...], [1], 1),
    ([..., 1, ...], [1],  1),
    ([1], [2], 0),
    ([1, 2], [1], 0),
    ([..., 1, 2, ...], [1, 2, 1, 2], 2)
])
def test_what(movement, path, count):
    assert Movement(movement).incidence_count(Path(path)) == count
