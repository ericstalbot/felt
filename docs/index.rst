.. include:: ../README.rst

Contents
========

.. toctree::
   :maxdepth: 2

   approach
   reference

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

