Reference
=========

Defining Networks
-----------------

The first step is to define a :class:`felt.network.Network`
using collections of :class:`felt.network.Node` and :class:`felt.network.Way`
instances.

.. autoclass:: felt.network.Node

.. autoclass:: felt.network.Way

.. autoclass:: felt.network.Network
    :exclude-members: shortest_path path


Defining Paths
--------------

The second step is to define :class:`felt.network.Path` instances
on the network. You can define these using the :meth:`felt.network.Network.path`
method or the :meth:`felt.network.Network.shortest_path` method. 

.. automethod:: felt.network.Network.path

.. automethod:: felt.network.Network.shortest_path


Defining Movements
------------------

The third step is to define :class:`felt.movement.Movement` instances. 

.. autoclass:: felt.movement.Movement
    :exclude-members: incidence_count   


Estimating Path Flows
---------------------

The last step is to estimate path flows using the :func:`felt.estimate.estimate`
function. 

.. autofunction:: felt.estimate.estimate

