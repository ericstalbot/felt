Approach
========

A network is a collection of nodes and links.
Networks are useful for modeling real-life systems; 
for example, a network may be used to model highways, 
where nodes are intersections, and links are the roads
between intersections. 

A path is a series of contiguous nodes on a network. 
There may be many paths on a network.
Each path starts at an origin node, passes through 
intermediate nodes, and ends at a destination node.
Paths may overlap, so multiple paths share 
a certain sub-sequence of nodes. 

Each path has a flow, which is a number. In the 
highway application it may be the number of vehicles
traveling on that path in an hour. Often the 
path flows are not directly observable. For example, 
we can count the number of vehicles traveling a 
single link (road segment); but because paths 
can overlap, those vehicles could be traveling 
on multiple paths, and we cannot tell how many vehicles
are traveling on each. 